#include <SFML/Graphics.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <tuple>
#include <math.h>

#include <cstdlib>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "lib/json.hpp"

using nlohmann::json;

void printerrno(const char * msg) {
	char error_str[1024];
	strerror_r(errno, error_str, 1024);
	fprintf(stderr, "%s: %s\n", msg, error_str);
}

void printerr(const char * msg) {
	fprintf(stderr, "%s\n", msg);
}

static int send_fds(int socket, int *fds, int n) {
	struct msghdr msg = {0};
	struct cmsghdr *cmsg;
	char buf[CMSG_SPACE(n * sizeof(int))], dup[256];
	memset(buf, '\0', sizeof(buf));
	struct iovec io = { .iov_base = &dup, .iov_len = sizeof(dup) };

	msg.msg_iov = &io;
	msg.msg_iovlen = 1;
	msg.msg_control = buf;
	msg.msg_controllen = sizeof(buf);

	cmsg = CMSG_FIRSTHDR(&msg);
	cmsg->cmsg_level = SOL_SOCKET;
	cmsg->cmsg_type = SCM_RIGHTS;
	cmsg->cmsg_len = CMSG_LEN(n * sizeof(int));

	memcpy ((int *) CMSG_DATA(cmsg), fds, n * sizeof (int));

	if (sendmsg(socket, &msg, 0) == -1) {
		perror("sendmsg");
		printerrno("sendmsg(fds)");
		return -1;
	}

	return 0;
}

const int MAX_WXH = 2048*2048;

struct window_memfd	{
	int img_memfd;
	uint8_t * img_data;
};

const int WIDTH = 1200;
const int HEIGHT = 800;

const int TITLEBAR_HEIGHT = 18;

sf::Font font;

template <typename T>
std::tuple<int, T> new_memfd_region(size_t size, bool seal_future_writes=false) {
	int memfd = memfd_create("windowsystem", MFD_ALLOW_SEALING);
	if (memfd == -1) {
		printerrno("memfd_create()");
		return {-1, nullptr};
	}
	
	if (ftruncate(memfd, size) == -1) {
		printerrno("ftruncate(memfd)");
		return {-1, nullptr};
	}

	// prevent attacker from shrinking buffer behind our backs and causing SIGBUS faults
	if (fcntl(memfd, F_ADD_SEALS, F_SEAL_SHRINK) == -1) {
		printerrno("fcntl(memfd, F_ADD_SEALS, F_SEAL_SHRINK)");
		return {-1, nullptr};
	}
	
	T data = (T) mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, 0);
	if (data == MAP_FAILED) {
		printerrno("mmap(memfd)");
		return {-1, nullptr};
	}

	if (seal_future_writes) {
		// adds F_SEAL_FUTURE_WRITE
		// existing mmaps are writeable but any future calls to mmap with PROT_WRITE will fail
		// used to give receivers a read-only view while we can still write to it
		// more info: https://patchwork.kernel.org/patch/10673437/
		if (fcntl(memfd, F_ADD_SEALS, F_SEAL_FUTURE_WRITE) == -1) {
			printerrno("fcntl(memfd, F_ADD_SEALS, F_SEAL_FUTURE_WRITE)");
			return {-1, nullptr};
		}
	}

	// no seals can be added/removed now
	if (fcntl(memfd, F_ADD_SEALS, F_SEAL_SEAL) == -1) {
		printerrno("fcntl(memfd, F_ADD_SEALS, F_SEAL_SEAL)");
		return {-1, nullptr};
	}
	
	memset(data, 0, size);

	return {memfd, data};
}

template <typename T>
inline T json_get_default(json j, std::string key, T default_value) {
	try {
		return j[key].get<T>();
	}
	catch (...) {
		return default_value;
	}
}

int id_counter = 0;

class Win {
public:
	std::string title;
	std::string uuid;
	
	int x; int y;
	int w; int h;
	
	int z_index;

	bool closed = false;
	bool decorated = true;
	
	sf::Text title_display;
	sf::RectangleShape titlebar;
	sf::Sprite window_contents;
	sf::Texture window_contents_texture;
	sf::RectangleShape window_whole;
	sf::Text close_btn;

	struct window_memfd winmemfd;
	int sock;
	char msgbuf[8192];

	int id;
	
	Win(std::string title, int x, int y, int w, int h, int z_index, bool decorated, 
		const std::string &uuid, const struct window_memfd &winmemfd, int sock) {
		this->title = title;
		this->uuid = uuid;
		this->x = x;
		this->y = y;
		this->w = w;
		this->h = h;
		this->z_index = z_index;
		this->decorated = decorated;
		this->sock = sock;
		title_display.setFont(font);
		title_display.setCharacterSize(16);
		title_display.setFillColor(sf::Color::White);
		title_display.setString(title);
		titlebar.setFillColor(sf::Color::Black);
		window_whole.setOutlineThickness(1);
		window_whole.setOutlineColor(sf::Color::Black);
		close_btn.setFont(font);
		close_btn.setCharacterSize(16);
		close_btn.setFillColor(sf::Color::White);
		close_btn.setString("x");
		window_contents_texture.create(w, h);
		window_contents.setTexture(window_contents_texture);
		this->move(x, y);
		this->resize(w, h);
		this->winmemfd = winmemfd;
		this->loadimage();
		id = id_counter++;
	};

	void move(int x, int y) {
		this->x = x;
		this->y = y;
		titlebar.setPosition(x, y);
		title_display.setPosition(x, y-(TITLEBAR_HEIGHT/4));
		window_contents.setPosition(x, y+TITLEBAR_HEIGHT);
		window_whole.setPosition(x, y);
		close_btn.setPosition(x+w-8, y-(TITLEBAR_HEIGHT/4));
	};

	void resize(int w, int h) {
		this->w = w;
		this->h = h;
		titlebar.setSize({(float)w, (float)TITLEBAR_HEIGHT});
		close_btn.setPosition(x+w-8, y-(TITLEBAR_HEIGHT/4));
		window_contents.setTextureRect({sf::Vector2i(0, 0), sf::Vector2i(w, h)});
		window_whole.setSize({(float)w, (float)(h+TITLEBAR_HEIGHT)});
	};

	void loadimage() {
		window_contents_texture.update((((uint8_t *)winmemfd.img_data) + 1), w, h, 0, 0);
	};

	void lclick(int x, int y) {
		snprintf(msgbuf, 8192, "lclick(%d, %d)", x, y);
		send(this->sock, msgbuf, 8192, 0);
	}

	int get_msg() {
		return recv(sock, msgbuf, 8192, 0);
	}

	~Win() {
		printf("closing window %s\n", this->title.c_str());
		send(this->sock, "close", 8192, 0);
		munmap(this->winmemfd.img_data, 1+(w*h));
		close(this->winmemfd.img_memfd);
		close(this->sock);
	}
};

std::vector<std::string> known_uuids;

const char * SOCK_PATH = "/tmp/windowsystem.socket";

int main() {

	char msgbuf[8192];

	int sock_s, sock_c;
	struct sockaddr_un local, remote;

	sock_s = socket(AF_UNIX, SOCK_SEQPACKET, 0);
	if (sock_s == -1) {
		printerrno("socket(sock_s)");
		return 1;
	}

	local.sun_family = AF_UNIX;
	snprintf(local.sun_path, sizeof(local.sun_path), SOCK_PATH);
	// unlinking now will remove the socket file when no process has it open
	unlink(local.sun_path);
	int len = strlen(local.sun_path) + sizeof(local.sun_family);
	int len_remote = sizeof(remote);
	
	if (bind(sock_s, (struct sockaddr *)&local, len) == -1) {
		printerrno("bind(sock_s)");
		exit(1);
	}

	if (listen(sock_s, 5) == -1) {
		printerrno("listen(sock_s)");
		exit(1);
	}

	if (fcntl(sock_s, F_SETFL, fcntl(sock_s, F_GETFL) | O_NONBLOCK) == -1) {
		printerrno("fcntl(sock_s, O_NONBLOCK)");
		exit(1);
	}

	sf::RenderWindow win (sf::VideoMode(WIDTH, HEIGHT), "windowsystem");
	win.setFramerateLimit(60);

	if (! font.loadFromFile("ProggyClean.ttf")) {
		printerr("error loading font");
		return 1;
	}

	int max_z_index = 1;

	std::vector<int> active_sockets;
	std::vector<int> sockets_to_retry;

	std::vector<std::unique_ptr<Win>> windows;

	int focused = -1;

	int dragging = -1;
	sf::Vector2i drag_start = {0, 0};

	int mousex, mousey;

	while (win.isOpen()) {
	sf::Event ev;
	while (win.pollEvent(ev)) {
		if (ev.type == sf::Event::Closed) {
			windows.clear();
			win.close();
		}
		if (ev.type == sf::Event::MouseButtonPressed) {
			if (ev.mouseButton.button == sf::Mouse::Left) {
				mousex = ev.mouseButton.x;
				mousey = ev.mouseButton.y;
				for (size_t i=0; i < windows.size(); i++) {
					// raise window
					if (windows[i]->window_whole.getGlobalBounds().contains(mousex, mousey)) {
						windows[i]->z_index = max_z_index + 1;
						max_z_index++;
					
						if (! windows[i]->decorated) {
							continue;
						}
						// close button
						if (windows[i]->close_btn.getGlobalBounds().contains(mousex, mousey)) {
							//windows_to_remove.insert(i);
							windows[i]->closed = true;
						}
						// dragging
						if (windows[i]->titlebar.getGlobalBounds().contains(mousex, mousey)) {
							dragging = windows[i]->id;
							windows[i]->z_index = max_z_index + 1;
							max_z_index++;
							drag_start.x = mousex - windows[i]->titlebar.getPosition().x;
							drag_start.y = mousey - windows[i]->titlebar.getPosition().y;
						}
						else {
							windows[i]->lclick(mousex - windows[i]->window_contents.getPosition().x, mousey - windows[i]->window_contents.getPosition().y);
						}
					}
				}
			}
		}
	}

	while ((sock_c = accept(sock_s, (struct sockaddr *)&remote, (socklen_t *)&len_remote)) != -1) {
		printf("new client connected: %d\n", sock_c);
		if (fcntl(sock_c, F_SETFL, fcntl(sock_c, F_GETFL) | O_NONBLOCK) == -1) {
			printerrno("fcntl(sock_c, O_NONBLOCK");
			continue;
		}
		if (std::find(active_sockets.begin(), active_sockets.end(), sock_c) == active_sockets.end()) {
			active_sockets.push_back(sock_c);
		}
	}
	
	for (int active_sock_c : active_sockets) {
		if (recv(active_sock_c, msgbuf, 8192, 0) == -1) {
			if (errno == EWOULDBLOCK) {
				if (std::find(sockets_to_retry.begin(), sockets_to_retry.end(), sock_c) == sockets_to_retry.end()) {
					sockets_to_retry.push_back(active_sock_c);
				}
				continue;
			} else {
				printerrno("recv(active_sock_c)");
				continue;
			}
		}
		
		// parse
		std::string action = "";
		std::string uuid = "";
		std::string title = "";
		int w = 0;
		int h = 0;
		int x = 0;
		int y = 0;
		bool decorated = true;
		
		json j;
		
		try {
			j = json::parse(msgbuf);
			action = j["action"].get<std::string>();
			uuid = j["uuid"].get<std::string>();
		} catch (...) {
			printerr("error parsing json (action, uuid)");
			printf("%s\n", msgbuf);
			continue;
		}
		
		if (action == "new-window") {
			try {
				title = j["title"].get<std::string>();
				w = j["w"].get<int>();
				h = j["h"].get<int>();
			}
			catch (...) {
				printerr("error parsing json (title, w, h)");
				continue;
			}

			x = json_get_default<int>(j, "x", 0);
			y = json_get_default<int>(j, "y", 0);
			decorated = json_get_default<bool>(j, "decorated", true);
		
			printf("new-window request from uuid=%s\n", uuid.c_str());
			if (std::find(known_uuids.begin(), known_uuids.end(), uuid) != known_uuids.end()) {
				printerr("attempt to create new window for existing uuid, ignored");
				continue;
			}

			if (w*h > MAX_WXH) {
				fprintf(stderr, "requested window size (%dx%d) is greater than max (2048x2048), ignored\n", w, h);
				continue;
			}

			if (w*h < 0) {
				fprintf(stderr, "requested window size (%dx%d) is invalid (negative), ignored\n", w, h);
				continue;
			}

			struct window_memfd winmemfd;

			std::tie(winmemfd.img_memfd, winmemfd.img_data) = new_memfd_region<uint8_t *>((1 + (w*h*4)));
			if (winmemfd.img_memfd == -1) {
				printerr("couldn't create img_memfd");
				continue;
			}

			int fds[1] = { winmemfd.img_memfd };
			if (send_fds(active_sock_c, fds, 1) == -1) {
				continue;
			}
			
			windows.push_back(std::make_unique<Win>(title, x, y, w, h, 0, decorated, uuid, winmemfd, active_sock_c));
			known_uuids.push_back(uuid);
		}

		else if (action == "list-windows") {
			json r = json::array();
			for (std::unique_ptr<Win>& win : windows) {
				json w;
				w["uuid"] = win->uuid;
				w["title"] = win->title;
				w["x"] = win->x;
				w["y"] = win->y;
				w["w"] = win->w;
				w["h"] = win->h;
				r.push_back(w);
			}
			std::string res = j.dump();
			send(active_sock_c, res.c_str(), res.size(), 0);
		}
	}

	for (int s : active_sockets) {
		if (std::find(sockets_to_retry.begin(), sockets_to_retry.end(), s) == sockets_to_retry.end()) {
			//printf("closing socket %d\n", s);
			//close(s);
		}
	}
	active_sockets.clear();
	
	active_sockets = sockets_to_retry;
	sockets_to_retry.clear();

	sf::Vector2i mousepos = sf::Mouse::getPosition(win);
	mousex = mousepos.x;
	mousey = mousepos.y;

	{
	auto it = windows.begin();
	while (it != windows.end()) {
		if ((*it)->winmemfd.img_data[0] == 1) {
			// marked for repainting
			(*it)->loadimage();
			(*it)->winmemfd.img_data[0] = 0;
		}
		if ((*it)->get_msg() != -1) {
			// new msg
			printf("new msg from uuid=%s: %s\n", (*it)->uuid.c_str(), (*it)->msgbuf);
			if (strncmp(((*it)->msgbuf), "close", 5) == 0) {
				(*it)->closed = true;
			}
		}
		if ((*it)->closed) {
			it = windows.erase(it);
		} else {
			it++;
		}
	}
	}

	{
	auto it = std::find_if(windows.begin(), windows.end(),
		[&] (std::unique_ptr<Win>& win) {
			return win->id == dragging;
		}
	);

	if (it != windows.end()) {
		if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
			(*it)->move(mousex - drag_start.x, mousey - drag_start.y);
		} else {
			dragging = -1;
		}
	}
	}

	focused = -1;

	// sort by z_index
	std::sort(windows.begin(), windows.end(),
		[](std::unique_ptr<Win>& a, std::unique_ptr<Win>& b) {
			return a->z_index < b->z_index;
		}
	);

	// focus follows mouse
	for (size_t i=0; i < windows.size(); i++) {
		if (windows[i]->window_whole.getGlobalBounds().contains(mousex, mousey)) {
			focused = windows[i]->id;
		}
		// normalize z_index
		windows[i]->z_index = i;
	}
	if (windows.size() != 0) {
		max_z_index = windows[windows.size()-1]->z_index;
	} else {
		max_z_index = 1;
	}

	win.clear(sf::Color::White);
	for (size_t i=0; i < windows.size(); i++) {
		if (focused == windows[i]->id) {
			windows[i]->window_whole.setOutlineColor(sf::Color::Blue);
		} else {
			windows[i]->window_whole.setOutlineColor(sf::Color::Black);
		}
		if (windows[i]->decorated) {
			win.draw(windows[i]->window_whole);
			win.draw(windows[i]->titlebar);
			win.draw(windows[i]->title_display);
			win.draw(windows[i]->close_btn);
		}
		win.draw(windows[i]->window_contents);
	}
	win.display();


	}  // while (win.isOpen())

	return 0;

}