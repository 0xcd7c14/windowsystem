windowsystem api
================

simple and complete example programs are given in apps, check them out.


creating a window
-----------------

  1. generate a UUID for your window
  2. open a nonblocking UNIX socket of type `SOCK_SEQPACKET`  (it's like a mix of `SOCK_STREAM` and `SOCK_DGRAM`: connection-based, ordered, reliable, but with message boundaries)
  3. connect to "/tmp/windowsystem.socket" and send a message (max 8192 chars, anything beyond will be discarded) in this format (json):

```
{
    "uuid": <your uuid>,
    "action": "new-window"
    "title": <window title>,
    "w": <width>,
    "h": <height>
}
```

  4. see [using the shared memory](#using-the-shared-memory)
  
  5. see [updating a window](#updating-a-window)

  NOTES:
    
  1. request to create a window using a UUID for which a window has already been created will be discarded.

  2. window size (w\*h) must be <= 2048\*2048, else request will be discarded.
    


updating a window
-----------------

  read [using the shared memory](#using-the-shared-memory) first.
    
  1. write your pixels to the `img` buffer, in the form of an 1D array of `uint8_t`s of length w\*h\*4 (RGBA)

  2. set the flag.



close window
------------

  read [using the shared memory](#using-the-shared-memory) first.

  if you want to close your window:

  1. send `close` on the socket.


  your window has been closed by the user:

  1. you'll get the same message: `close` on the socket.

  your window is now closed. you can exit if you want, etc. further updates to `img` and the socket will be ignored.



using the shared memory
-----------------------

  when you create a window, you are given a shared memory buffer, called `img`.

  this will be given to on the Unix socket you used to send the new-window request. call `recvmsg()` on the socket and parse the 
  `struct msghdr` to extract them. example function to do this:

```c
static void recv_fds(int socket, int * fds, int n) {
    struct msghdr msg = {0};
    struct cmsghdr *cmsg;
    char buf[CMSG_SPACE(n * sizeof(int))], dup[256];
    memset(buf, '\0', sizeof(buf));
    struct iovec io = { .iov_base = &dup, .iov_len = sizeof(dup) };

    msg.msg_iov = &io;
    msg.msg_iovlen = 1;
    msg.msg_control = buf;
    msg.msg_controllen = sizeof(buf);

    if (recvmsg(socket, &msg, 0) == -1) {
        perror("recvmsg(fds)");
        return;
    }

    cmsg = CMSG_FIRSTHDR(&msg);
    if (! cmsg) {
        fprintf(stderr, "invalid cmsg\n");
        return;
    }
    memcpy (fds, (int *) CMSG_DATA(cmsg), n * sizeof(int));
}
// ...

int img_memfd = -1;
recv_fds(sock, &img_memfd, 1);
```

  now that you have the `fd`, you need to `mmap()` (memory map) it to actually read and write into it.

  example:

```c
size_t MEMFD_SIZE = 1 + w*h*4;  // enough for wxh pixels + flag byte
uint8_t * img_data = (uint8_t *) mmap(NULL, MEMFD_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, img_memfd, 0);
```

  the size of `img` is `1 + width*height*4`.

  the first byte of `img` is a flag used to indicate new data. if you have written new data into the buffers 
  and would like windowsystem to read it, set the first byte to 1 (example: `img[0] = 1`), and windowsystem will notice 
  the flag, read the data, etc, and will then set the flag to 0 again, to indicate it has read it.

  when writing/reading into the buffers, take care that the actual data starts from the second byte.

  note that the memfds are sealed with `F_SEAL_SHRINK` and `F_SEAL_SEAL`, so attempts to `ftruncate()` them or change their seals will fail.