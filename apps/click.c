#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <mqueue.h>
#include <errno.h>

#include <uuid/uuid.h>

const int WIDTH = 100;
const int HEIGHT = 100;
const size_t IMG_MEMFD_SIZE = (1 + WIDTH * HEIGHT * 4);
const char * TITLE = "click";
const char * SOCK_PATH = "/tmp/windowsystem.socket";

uuid_t uuid;
char uuid_str[37];
char window_create_msg[8192];

uint8_t * img_data;
int sock;
int connected = 0;

static void recv_fds(int socket, int * fds, int n) {
	struct msghdr msg = {0};
	struct cmsghdr *cmsg;
	char buf[CMSG_SPACE(n * sizeof(int))], dup[256];
	memset(buf, '\0', sizeof(buf));
	struct iovec io = { .iov_base = &dup, .iov_len = sizeof(dup) };

	msg.msg_iov = &io;
	msg.msg_iovlen = 1;
	msg.msg_control = buf;
	msg.msg_controllen = sizeof(buf);

	if (recvmsg(socket, &msg, 0) == -1) {
		perror("recvmsg(fds)");
		return;
	}

	cmsg = CMSG_FIRSTHDR(&msg);
	if (! cmsg) {
		fprintf(stderr, "invalid cmsg\n");
		return;
	}
	memcpy (fds, (int *) CMSG_DATA(cmsg), n * sizeof(int));
}

void cleanup() {
	printf("cleaning up...\n");
	if (connected == 1) {
		// send close msg
		send(sock, "close", 8192, 0);
		printf("sent: close\n");
		close(sock);
	}
}

void siginthandler(int sig) {
	exit(1);
}

int main() {
	
	atexit(cleanup);
	signal(SIGINT, siginthandler);
	
	uuid_generate(uuid);
	uuid_unparse_lower(uuid, uuid_str);
	
	snprintf(window_create_msg, 8192, "{\"action\":\"new-window\",\"title\":\"%s\",\"uuid\":\"%s\",\"w\":%d,\"h\":%d}",
			 TITLE, uuid_str, WIDTH, HEIGHT);
	printf("%s\n", window_create_msg);

	int fds[1] = {-1};

	struct sockaddr_un remote;

	if ((sock = socket(AF_UNIX, SOCK_SEQPACKET, 0)) == -1) {
		perror("socket()");
		return 1;
	}

	printf("connecting to windowsystem socket...\n");

	remote.sun_family = AF_UNIX;
	snprintf(remote.sun_path, sizeof(remote.sun_path), SOCK_PATH);
	int len = strlen(remote.sun_path) + sizeof(remote.sun_family);
	if (connect(sock, (struct sockaddr *)&remote, len) == -1) {
		perror("connect()");
		return 1;
	}

	printf("connected\n");

	if (send(sock, window_create_msg, 8192, 0) == -1) {
		perror("send()");
		return 1;
	}

	recv_fds(sock, fds, 1);

	img_data = (uint8_t *) mmap(NULL, IMG_MEMFD_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fds[0], 0);
	if (img_data == MAP_FAILED) {
		perror("mmap(img)");
		return 1;
	}

	// first byte is used for flag
	img_data++;

	connected = 1;
	
	memset(img_data, 255, WIDTH*HEIGHT);
	*(img_data - 1) = 1;

	int x = 0;
	int y = 0;
	char msgbuf[8192];

	while (1) {
		if (recv(sock, msgbuf, 8192, 0) == -1) {
			perror("recv(sock)");
		} else {
			if (strncmp(msgbuf, "close", 5) == 0) {
				printf("received: close\n");
				exit(0);
			} else if (strncmp(msgbuf, "lclick", 6) == 0) {
				sscanf(msgbuf, "lclick(%d, %d)", &x, &y);
				size_t base = (x + WIDTH * y) * 4;
				img_data[base + 0] = 255;
				img_data[base + 1] = 0;
				img_data[base + 2] = 0;
				img_data[base + 3] = 255;
				*(img_data - 1) = 1;
			}
		}
	}
}