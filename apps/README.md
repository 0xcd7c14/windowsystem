windowsystem example apps
=========================

these are some example apps using the windowsystem API.

make sure to run windowsystem *before* you run the apps.


rgbflasher
----------

simple window which flashes red, green, and blue at intervals of 2 seconds.

```sh
$ make rgbflasher
$ ./rgbflasher
```


gifplayer
---------

plays GIF animations. (loop delay might be wrong on some GIFs)

```sh
$ make gifplayer
$ ./gifplayer /path/to/image.gif
```


maltest
-------

sends intentionally malformed input to windowsystem to test for security issues.

available tests:

  - `buffershrink` - attempts to shrink the memfd buffer to cause SIGBUS faults in windowsystem
  - `socketoverflow` - attempts to send >8192 bytes of data over the socket request
  - `socketnorecv` - closes socket connection without send()ing
  - `socketsendwait` - refuses to send() data on socket connection but without closing connection
  - `socketnorecv` - closes socket connection without recv()ing
  - `socketrecvwait` - refuses to recv() data on socket connection but without closing connection
  - `largewindow` - attempts to create an enormous window (4096x4096)
  - `negativesize` - attempts to create a negative size window (-100x100) to cause a huge allocation when converted to unsigned size_t
  - `randomimage` - fills the img buffer with random data

currently all of these tests pass (windowsystem proceeds normally and doesn't crash/freeze or have a buffer overflow)

```sh
$ make maltest
$ ./maltest <test>
```