#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <mqueue.h>
#include <time.h>
#include <errno.h>

#include <uuid/uuid.h>

#include "lib/gifdec.h"

const char * TITLE = "gifplayer";

const char * SOCK_PATH = "/tmp/windowsystem.socket";

uuid_t uuid;
char uuid_str[37];
char window_create_msg[8192];

gd_GIF * gif;
uint8_t * rgb_data;

int img_memfd = -1;
uint8_t * img_data;
int sock;
char msgbuf[8192];

int gif_inited = 0;
int rgb_data_inited = 0;

int connected = 0;

static void recv_fds(int socket, int * fds, int n) {
	struct msghdr msg = {0};
	struct cmsghdr *cmsg;
	char buf[CMSG_SPACE(n * sizeof(int))], dup[256];
	memset(buf, '\0', sizeof(buf));
	struct iovec io = { .iov_base = &dup, .iov_len = sizeof(dup) };

	msg.msg_iov = &io;
	msg.msg_iovlen = 1;
	msg.msg_control = buf;
	msg.msg_controllen = sizeof(buf);

	if (recvmsg(socket, &msg, 0) == -1) {
		perror("recvmsg(fds)");
		return;
	}

	cmsg = CMSG_FIRSTHDR(&msg);
	if (! cmsg) {
		fprintf(stderr, "invalid cmsg\n");
		return;
	}
	memcpy (fds, (int *) CMSG_DATA(cmsg), n * sizeof(int));
}

void cleanup() {
	printf("cleaning up...\n");
	if (gif_inited == 1) {
		gd_close_gif(gif);
	}
	if (rgb_data_inited == 1) {
		free(rgb_data);
	}
	if (connected == 1) {
		// send close msg
		send(sock, "close", 8192, 0);
		printf("sent: close\n");
		close(sock);
		close(img_memfd);
	}
}

void siginthandler(int sig) {
	exit(1);
}

int main(int argc, char * argv[]) {
	
	atexit(cleanup);
	signal(SIGINT, siginthandler);

	if (argc < 2) {
		fprintf(stderr, "usage: %s gif-file-path\n", argv[0]);
		return 1;
	}
	
	gif = gd_open_gif(argv[1]);
	gif_inited = 1;
	
	uuid_generate(uuid);
	uuid_unparse_lower(uuid, uuid_str);
	
	snprintf(window_create_msg, 8192, "{\"action\":\"new-window\",\"title\":\"%s\",\"uuid\":\"%s\",\"w\":%d,\"h\":%d}",
			 TITLE, uuid_str, gif->width, gif->height);
	printf("%s\n", window_create_msg);

	int sock;
	struct sockaddr_un remote;

	if ((sock = socket(AF_UNIX, SOCK_SEQPACKET, 0)) == -1) {
		perror("socket()");
		return 1;
	}

	printf("connecting to windowsystem socket...\n");

	remote.sun_family = AF_UNIX;
	snprintf(remote.sun_path, sizeof(remote.sun_path), SOCK_PATH);
	int len = strlen(remote.sun_path) + sizeof(remote.sun_family);
	if (connect(sock, (struct sockaddr *)&remote, len) == -1) {
		perror("connect()");
		return 1;
	}

	printf("connected\n");

	if (send(sock, window_create_msg, 8192, 0) == -1) {
		perror("send()");
		return 1;
	}

	recv_fds(sock, &img_memfd, 1);

	rgb_data = malloc(gif->width * gif->height * 3);
	rgb_data_inited = 1;

	img_data = (uint8_t *) mmap(NULL, (1+(gif->width * gif->height * 4)), PROT_READ | PROT_WRITE, MAP_SHARED, img_memfd, 0);
	if (img_data == MAP_FAILED) {
		perror("mmap(img)");
		return 1;
	}

	// first byte is used for flag
	img_data++;

	connected = 1;
	int running = 1;

	if (fcntl(sock, F_SETFL, fcntl(sock, F_GETFL) | O_NONBLOCK) == -1) {
		perror("fcntl(sock, O_NONBLOCK)");
		return 1;
	}
	
	for (int y = 0; y < gif->height; y++) {
		for (int x = 0; x < gif->width; x++) {
			size_t base = (x + gif->width * y) * 4;
			img_data[base + 3] = 255;
		}
	}

	for (unsigned looped = 1;; looped++) {
		
		if (recv(sock, msgbuf, 8192, 0) == -1) {
			if (errno != EWOULDBLOCK) {
				perror("recv(sock)");
			}
		} else {
			if (strncmp(msgbuf, "close", 5) == 0) {
				printf("received: close\n");
				exit(0);
			} else if (strncmp(msgbuf, "lclick", 6) == 0) {
				running = !running;
			}
		}
		
		if (running) {
			while (gd_get_frame(gif)) {
				gd_render_frame(gif, rgb_data);
				
				// convert rgb to rgba
				for (int y = 0; y < gif->height; y++) {
					for (int x = 0; x < gif->width; x++) {
						size_t rgb_base  = (x + gif->width * y) * 3;
						size_t rgba_base = (x + gif->width * y) * 4;
						img_data[rgba_base + 0] = rgb_data[rgb_base + 0];
						img_data[rgba_base + 1] = rgb_data[rgb_base + 1];
						img_data[rgba_base + 2] = rgb_data[rgb_base + 2];
					}
				}
				
				// set repaint flag
				*(img_data - 1) = 1;
				
				long nano = gif->gce.delay * 1000000L;
				nanosleep((const struct timespec[]){{0, nano}}, NULL);
			}
			
			if (looped == gif->loop_count) {
				break;
			}
			gd_rewind(gif);
		}		
	}
}