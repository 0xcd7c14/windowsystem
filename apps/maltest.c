#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <mqueue.h>
#include <errno.h>

#include <uuid/uuid.h>

const size_t MSG_MEMFD_SIZE = (1 + 8192);
const char * TITLE = "maltest";
const char * SOCK_PATH = "/tmp/windowsystem.socket";

uuid_t uuid;
char uuid_str[37];
char window_create_msg[16384];

uint8_t * img_data;
int img_memfd;
int sock;
char msgbuf[8192];
int connected = 0;

static void recv_fds(int socket, int * fds, int n) {
	struct msghdr msg = {0};
	struct cmsghdr *cmsg;
	char buf[CMSG_SPACE(n * sizeof(int))], dup[256];
	memset(buf, '\0', sizeof(buf));
	struct iovec io = { .iov_base = &dup, .iov_len = sizeof(dup) };

	msg.msg_iov = &io;
	msg.msg_iovlen = 1;
	msg.msg_control = buf;
	msg.msg_controllen = sizeof(buf);

	if (recvmsg(socket, &msg, 0) == -1) {
		perror("recvmsg(fds)");
		return;
	}

	cmsg = CMSG_FIRSTHDR(&msg);
	if (! cmsg) {
		fprintf(stderr, "invalid cmsg\n");
		return;
	}
	memcpy (fds, (int *) CMSG_DATA(cmsg), n * sizeof(int));
}

int main(int argc, char * argv[]) {
	
	if (argc < 2) {
		printf("maltest - sends intentionally malformed input to windowsystem to test for security issues\n\n");
		printf("usage: %s <test>\n\n", argv[0]);
		printf("available tests:\n");
		printf("  buffershrink - attempts to shrink the memfd buffer to cause SIGBUS faults in windowsystem\n");
		printf("  socketoverflow - attempts to send >8192 bytes of data over the socket request\n");
		printf("  socketnosend - closes socket connection without send()ing\n");
		printf("  socketsendwait - refuses to send() data on socket connection but without closing connection\n");
		printf("  socketnorecv - closes socket connection without recv()ing\n");
		printf("  socketrecvwait - refuses to recv() data on socket connection but without closing connection\n");
		printf("  largewindow - attempts to create an enormous window (4096x4096)\n");
		printf("  negativesize - attempts to create a negative size window (-100x100) to cause a huge allocation when converted to unsigned size_t\n");
		printf("  randomimage - fills the img buffer with random data\n");
		return 1;
	}

	printf("%s\n", argv[1]);
	
	uuid_generate(uuid);
	uuid_unparse_lower(uuid, uuid_str);

	if (strncmp(argv[1], "socketoverflow", strlen("socketoverflow")) == 0) {
		printf("filling window_create_msg...\n");
		memset(window_create_msg, 'A', 16384);
		window_create_msg[16384 - 1] = '\0';
		window_create_msg[16384 - 4] = 'E';
		window_create_msg[16384 - 3] = 'N';
		window_create_msg[16384 - 2] = 'D';
	}

	int width = 100;
	int height = 100;
	
	if (strncmp(argv[1], "largewindow", strlen("largewindow")) == 0) {
		width = 4096;
		height = 4096;
	}
	
	if (strncmp(argv[1], "negativesize", strlen("negativesize")) == 0) {
		width = -100;
		height = 100;
	}
	
	int retsize = snprintf(window_create_msg, 8192, "{\"action\":\"new-window\",\"title\":\"%s\",\"uuid\":\"%s\",\"w\":%d,\"h\":%d}",
		TITLE, uuid_str, width, height);
	
	const size_t IMG_MEMFD_SIZE = (1 + width * height * 4);

	printf("expected img memfd size allocation: %lu\n", IMG_MEMFD_SIZE);

	if (strncmp(argv[1], "socketoverflow", strlen("socketoverflow")) == 0) {
		window_create_msg[retsize] = 'A';
		printf("strlen(window_create_msg) = %lu\n", strlen(window_create_msg));
	}
	
	printf("%s\n", window_create_msg);

	int sock;
	struct sockaddr_un remote;

	if ((sock = socket(AF_UNIX, SOCK_SEQPACKET, 0)) == -1) {
		perror("socket()");
		return 1;
	}

	printf("connecting to windowsystem socket...\n");

	remote.sun_family = AF_UNIX;
	snprintf(remote.sun_path, sizeof(remote.sun_path), SOCK_PATH);
	int len = strlen(remote.sun_path) + sizeof(remote.sun_family);
	if (connect(sock, (struct sockaddr *)&remote, len) == -1) {
		perror("connect()");
		return 1;
	}

	printf("connected\n");

	if (strncmp(argv[1], "socketnosend", strlen("socketnosend")) == 0) {
		close(sock);
		return 1;
	}

	if (strncmp(argv[1], "socketsendwait", strlen("socketsendwait")) == 0) {
		printf("press enter to send()");
		getchar();
		printf("sending now...\n");
	}

	if (send(sock, window_create_msg, 8192, 0) == -1) {
		perror("send()");
		return 1;
	}

	if (strncmp(argv[1], "socketrecvwait", strlen("socketrecvwait")) == 0) {
		printf("press enter to recv()");
		getchar();
		printf("recv()ing now...\n");
	}

	if (strncmp(argv[1], "socketnorecv", strlen("socketnorecv")) == 0) {
		close(sock);
		return 1;
	}

	recv_fds(sock, &img_memfd, 2);

	close(sock);

	if (strncmp(argv[1], "buffershrink", strlen("buffershrink")) == 0) {
		printf("trying to shrink img memfd buffer...\n");
		ftruncate(img_memfd, 4);
		perror("ftruncate");
	}

	img_data = (uint8_t *) mmap(NULL, IMG_MEMFD_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, img_memfd, 0);
	if (img_data == MAP_FAILED) {
		perror("mmap(img)");
		return 1;
	}

	// first byte is used for flag
	img_data++;

	connected = 1;

	if (strncmp(argv[1], "randomimage", strlen("randomimage")) == 0) {
		printf("writing /dev/random data to img...\n");
		int fd = open("/dev/random", O_RDONLY);
		read(fd, img_data, IMG_MEMFD_SIZE-1);
		close(fd);
	}

	*(img_data - 1) = 1;

	while (1) {
		recv(sock, msgbuf, 8192, 0);
	}
}