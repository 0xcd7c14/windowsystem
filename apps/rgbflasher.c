#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <mqueue.h>
#include <errno.h>

#include <uuid/uuid.h>

const int WIDTH = 100;
const int HEIGHT = 100;
const size_t IMG_MEMFD_SIZE = (1 + WIDTH * HEIGHT * 4);
const char * TITLE = "rgb";
const char * SOCK_PATH = "/tmp/windowsystem.socket";

uuid_t uuid;
char uuid_str[37];
char window_create_msg[8192];

uint8_t * img_data;
int img_memfd;
int sock;
char msgbuf[8192];

int connected = 0;

static void recv_fds(int socket, int * fds, int n) {
	struct msghdr msg = {0};
	struct cmsghdr *cmsg;
	char buf[CMSG_SPACE(n * sizeof(int))], dup[256];
	memset(buf, '\0', sizeof(buf));
	struct iovec io = { .iov_base = &dup, .iov_len = sizeof(dup) };

	msg.msg_iov = &io;
	msg.msg_iovlen = 1;
	msg.msg_control = buf;
	msg.msg_controllen = sizeof(buf);

	if (recvmsg(socket, &msg, 0) == -1) {
		perror("recvmsg(fds)");
		return;
	}

	cmsg = CMSG_FIRSTHDR(&msg);
	if (! cmsg) {
		fprintf(stderr, "invalid cmsg\n");
		return;
	}
	memcpy (fds, (int *) CMSG_DATA(cmsg), n * sizeof(int));
}

void cleanup() {
	printf("cleaning up...\n");
	if (connected == 1) {
		// send close msg
		send(sock, "close", 8192, 0);
		printf("sent: close\n");
		close(sock);
	}
}

void siginthandler(int sig) {
	exit(1);
}

int main() {
	
	atexit(cleanup);
	signal(SIGINT, siginthandler);
	
	uuid_generate(uuid);
	uuid_unparse_lower(uuid, uuid_str);
	
	snprintf(window_create_msg, 8192, "{\"action\":\"new-window\",\"title\":\"%s\",\"uuid\":\"%s\",\"w\":%d,\"h\":%d}",
			 TITLE, uuid_str, WIDTH, HEIGHT);
	printf("%s\n", window_create_msg);

	int sock;
	struct sockaddr_un remote;

	if ((sock = socket(AF_UNIX, SOCK_SEQPACKET, 0)) == -1) {
		perror("socket()");
		return 1;
	}

	printf("connecting to windowsystem socket...\n");

	remote.sun_family = AF_UNIX;
	snprintf(remote.sun_path, sizeof(remote.sun_path), SOCK_PATH);
	int len = strlen(remote.sun_path) + sizeof(remote.sun_family);
	if (connect(sock, (struct sockaddr *)&remote, len) == -1) {
		perror("connect()");
		return 1;
	}

	printf("connected\n");

	if (send(sock, window_create_msg, 8192, 0) == -1) {
		perror("send()");
		return 1;
	}

	recv_fds(sock, &img_memfd, 1);

	img_data = (uint8_t *) mmap(NULL, IMG_MEMFD_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, img_memfd, 0);
	if (img_data == MAP_FAILED) {
		perror("mmap(img)");
		return 1;
	}

	// first byte is used for flag
	img_data++;

	connected = 1;

	if (fcntl(sock, F_SETFL, fcntl(sock, F_GETFL) | O_NONBLOCK) == -1) {
		perror("fcntl(sock, O_NONBLOCK)");
		return 1;
	}
	
	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x++) {
			size_t base = (x + WIDTH * y) * 4;
			img_data[base + 3] = 255;
		}
	}

	int rgb = 0;
	while (1) {
		
		if (recv(sock, msgbuf, 8192, 0) == -1) {
			if (errno != EWOULDBLOCK) {
				perror("recv(sock)");
			}
		} else {
			if (strncmp(msgbuf, "close", 5) == 0) {
				printf("received: close\n");
				exit(0);
			}
		}
		
		for (int y = 0; y < HEIGHT; y++) {
			for (int x = 0; x < WIDTH; x++) {
				size_t base = (x + WIDTH * y) * 4;
				if (rgb == 0) {
					img_data[base + 0] = 255;
					img_data[base + 1] = 0;
					img_data[base + 2] = 0;
				}
				if (rgb == 1) {
					img_data[base + 0] = 0;
					img_data[base + 1] = 255;
					img_data[base + 2] = 0;
				}
				if (rgb == 2) {
					img_data[base + 0] = 0;
					img_data[base + 1] = 0;
					img_data[base + 2] = 255;
				}
			}
		}
		
		// set repaint flag
		*(img_data - 1) = 1;
		
		sleep(2);
		
		rgb++;
		if (rgb > 2) {
			rgb = 0;
		}
	}
}