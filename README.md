windowsystem
============

a simple fast window system (like X11), as a proof of concept
-------------------------------------------------------------

![windowsystem demo](./img/windowsystem.webm)

requirements:
  - Linux >= 4.2.0
  - SFML

```sh
$ make windowsystem
$ ./windowsystem
```

want to write programs for windowsystem? read the [docs](./api.md) and check out the [examples](./apps).

TODO:

  - allow undecorated windows
  - input events other than close

License: MIT